from django.urls import path
from api.views import *
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    path('/todos' , ClassBasedApiView.as_view()),
    path('/todos/<int:pk>', ClassBasedApiView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)