from api.models import Todo
from rest_framework import serializers


class TodoSerializer(serializers.ModelSerializer):


    class Meta:
        model = Todo
        fields = ['id','nome','done','created_at']