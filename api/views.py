from api.serializers import TodoSerializer
from rest_framework.response import Response
from api.models import Todo
from rest_framework.views import APIView
from rest_framework import status

class ClassBasedApiView(APIView):

    """
    Lista all todo's
    """

    def get(self,request,*args,**kwargs):
        todo = Todo.objects.all()
        serializer = TodoSerializer(todo, many=True)
        return Response(serializer.data)
    
    """
    Creating one todo
    """

    def post(self, request , *args, **kwargs):
        serializer = TodoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)     
        return Response(serializer.errorsm, status=status.HTTP_404_BAD_REQUEST)
    
    def put( self, request, pk, *args, **kwargs):
        """
        alter register
        """
        try:
            get_todo = Todo.objects.get(pk=pk)
        except Todo.DoesNotExist:
            return Response(status=status.HTTP_404_BAD_REQUEST)
        
        serializer = TodoSerializer(get_todo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

    def delete(self, request, pk,*args, **kwargs):
        """
        delete this register
        """
        try:
            get_todo = Todo.objects.get(pk=pk)
        except Todo.DoesNotExist:
            return Response(status=status.HTTP_404_BAD_REQUEST)
        
        get_todo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

        
            

            


   



